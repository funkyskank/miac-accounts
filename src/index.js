import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';

import StyleThemeProvider from 'containers/StyleThemeProvider';
import './globalStyles';

import App from './App';

import configureStore from './configureStore';

const store = configureStore({});

ReactDOM.render(
  <Provider store={store}>
    <StyleThemeProvider>
      <App />
    </StyleThemeProvider>
  </Provider>,
  document.getElementById('root'),
);
