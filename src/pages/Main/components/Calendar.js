import React from 'react';
import styled, { css } from 'styled-components';

const Wrapper = styled.div`

`;

const Calendar = styled.table`
  width: 100%;
  font-size: 0.875rem;
  table-layout: fixed;
  border-collapse: collapse;
  border-style: hidden;

  & th {
  	font-weight: 500;
  	height: 87px;
  	color: ${({ theme }) => theme.colors.subtitle};
  	text-transform: uppercase;
  	border: 1px solid ${({ theme }) => theme.colors.border};
  }

  & td {
  	height: 123px;
  	color: ${({ theme }) => theme.colors.subtitle};
  	border: 1px solid ${({ theme }) => theme.colors.border};
  	text-align: right;
  	vertical-align: top;
  }
`;

const Day = styled.div`
  margin: 5px;
  display: inline-block;
  padding: 3px 5px;

  ${({ today }) => today && css`
    color: #fdfdfd;
    background-color: rgb(248, 63, 58);
    border-radius: 4px;
  `}
`;

const colorMapper = {
  green: {
    color: 'rgb(46, 167, 82)',
    bgColor: 'rgb(226, 249, 219)',
  },
  pink: {
    color: 'rgb(223, 65, 116)',
    bgColor: 'rgb(255, 212, 226)',
  },
  orange: {
    color: 'rgb(223, 130, 80)',
    bgColor: 'rgb(254, 238, 208)',
  },
  purple: {
    color: 'rgb(157, 97, 157)',
    bgColor: 'rgb(247, 227, 249)',
  },
  blue: {
    color: 'rgb(60, 132, 190)',
    bgColor: 'rgb(211, 240, 254)',
  },
};

const Event = styled.div`
  font-size: 0.75rem;
  font-weight: 600;
  margin: 5px;
  padding: 5px 10px;
  border-radius: 5px;
  text-align: left;
  cursor: pointer;

  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;

  ${({ color }) => color && css`
    color: ${colorMapper[color].color};
    background-color: ${colorMapper[color].bgColor};
  `}
`;

const CalendarSection = () => (
  <Wrapper>
    <Calendar>
      <tbody>
        <tr>
          <th>Sunday</th>
          <th>Monday</th>
          <th>Tuesday</th>
          <th>Wednesday</th>
          <th>Thursday</th>
          <th>Friday</th>
          <th>Saturday</th>
        </tr>
        <tr>
          <td><Day>28</Day></td>
          <td>
            <Day>29</Day>
            <Event color="purple">Pictures in the sky</Event>
            <Event color="orange">Murder of the Universe</Event>
            <Event color="green">The amazing hubble...</Event>
          </td>
          <td><Day>30</Day></td>
          <td>
            <Day>31</Day>
            <Event color="green">Pictures in the sky</Event>
          </td>
          <td><Day>1</Day></td>
          <td><Day>2</Day></td>
          <td><Day>3</Day></td>
        </tr>
        <tr>
          <td><Day>4</Day></td>
          <td><Day>5</Day></td>
          <td><Day>6</Day></td>
          <td>
            <Day>7</Day>
            <Event color="pink">Branding do you know</Event>
          </td>
          <td>
            <Day>8</Day>
            <Event color="orange">Top 3 Reasons Why...</Event>
          </td>
          <td><Day today>9</Day></td>
          <td><Day>10</Day></td>
        </tr>
        <tr>
          <td><Day>11</Day></td>
          <td><Day>12</Day></td>
          <td><Day>13</Day></td>
          <td><Day>14</Day></td>
          <td><Day>15</Day></td>
          <td>
            <Day>16</Day>
            <Event color="purple">The amazing hubble...</Event>
          </td>
          <td><Day>17</Day></td>
        </tr>
        <tr>
          <td><Day>18</Day></td>
          <td>
            <Day>19</Day>
            <Event color="pink">The amazing hubble...</Event>
          </td>
          <td><Day>20</Day></td>
          <td>
            <Day>21</Day>
            <Event color="green">King Gizzard and the Lizard Wizard</Event>
          </td>
          <td><Day>22</Day></td>
          <td><Day>23</Day></td>
          <td><Day>24</Day></td>
        </tr>
        <tr>
          <td><Day>25</Day></td>
          <td><Day>26</Day></td>
          <td>
            <Day>27</Day>
            <Event color="blue">Global travel and vacations luxury</Event>
          </td>
          <td><Day>28</Day></td>
          <td><Day>29</Day></td>
          <td><Day>30</Day></td>
          <td><Day>1</Day></td>
        </tr>
      </tbody>
    </Calendar>
  </Wrapper>
);

export default CalendarSection;
