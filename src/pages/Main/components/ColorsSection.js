import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  padding: 30px;
`;

const ElementContainer = styled.div`
  flex: 1 1 15%;
  min-width: 150px;
  display: flex;
  align-items: center;
  margin: 20px;
`;

const Ball = styled.div`
  width: 50px;
  height: 50px;
  border-radius: 50%;
  background-color: ${props => props.color};
`;

const Description = styled.div`
  margin-left: 40px;
  font-size: 0.875rem;
  color: #b3b3b3;
`;

const ColorName = styled.div`
  text-transform: capitalize;
  margin-bottom: 10px;
`;

const ColorValue = styled.div`
  font-size: 0.75rem;
`;

const colors = {
  '98 Gray': '#FAFAFA',
  '96 Gray': '#F5F5F5',
  '94 Gray': '#F0F0F0',
  '90 Gray': '#E6E6E6',
  '85 Gray': '#D9D9D9',
  '80 Gray': '#CCCCCC',
  '70 Gray': '#B3B3B3',
  '60 Gray': '#999999',
  '50 Gray': '#808080',
  '40 Gray': '#666666',
  '30 Gray': '#4D4D4D',
  '20 Gray': '#333333',
  '15 Gray': '#262626',
  '10 Gray': '#1A1A1A',
  '5 Gray': '#0D0D0D',
  blue: '#1875F0',
  green: '#50D166',
  red: '#F13A30',
  orange: '#F18F1C',
  purple: '#5553CE',
};

const ColorsSection = () => (
  <Wrapper>
    {Object.keys(colors).map((color, index) => (
      <ElementContainer key={index}>
        <Ball color={colors[color]} />
        <Description>
          <ColorName>{color}</ColorName>
          <ColorValue>{colors[color]}</ColorValue>
        </Description>
      </ElementContainer>
    ))}
  </Wrapper>
);

export default ColorsSection;
