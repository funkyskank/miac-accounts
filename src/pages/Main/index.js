import React from 'react';
import styled, { css } from 'styled-components';
import { Grid, Cell } from 'components/Grid';
import Input from 'components/Input';
import water from 'images/water.jpg';

import ColorsSection from './components/ColorsSection';
import Calendar from './components/Calendar';

const Container = styled.div`
  margin: 0 auto;
  padding-top: 95px;
  max-width: 1280px;
`;

const Content = styled.div`
  width: 100%;
  background-color: #fdfdfd;
  border-radius: 3px;
  box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.1);
  margin: 0 5px 10px;
  padding: 10px;
  text-align: center;
`;

const Section = styled.div`
  width: 100%;
  background-color: #fdfdfd;
  border-radius: 3px;
  box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.1);
  margin: 0 5px 10px;
  padding: 0;

  ${({ primary }) => primary && css`
    background-color: rgb(8, 111, 233);
    color: #fff;
    & > div:first-child {
      border-bottom: 2px solid rgba(255,255,255,.2);
    }
  `}
`;

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 1.25rem 1.5rem;
  border-bottom: 2px solid ${({ theme }) => theme.colors.border};
`;

const SectionContent = styled.div`
  padding: 1.25rem 1.5rem;
  min-height: 300px;
`;

const InputRow = styled.div`
  display: flex;
  justify-content: space-between;

  margin-bottom: 24px;
`;

const FullWidthInput = styled(Input)`
  width: 100%;

  :not(:last-child) {
    margin-right: 24px;
  }
`;

const Main = () => (
  <Container>
    <Grid>
      <Cell width={1 / 2}>
        <Content>1/2</Content>
      </Cell>
      <Cell>
        <Content>auto</Content>
      </Cell>
      <Cell width={1 / 3}>
        <Content>1/3</Content>
      </Cell>
      <Cell width={1}>
        <Content>full</Content>
      </Cell>
    </Grid>

    <Grid>
      <Cell>
        <Section>
          <Header>Section Header</Header>
          <SectionContent>Content</SectionContent>
        </Section>
      </Cell>
    </Grid>

    <Grid>
      <Cell>
        <Section>
          <Header>Colors</Header>
          <SectionContent>
            <ColorsSection />
          </SectionContent>
        </Section>
      </Cell>
    </Grid>

    <Grid>
      <Cell>
        <Section>
          <Calendar />
        </Section>
      </Cell>
    </Grid>

    <Grid style={{ flexWrap: 'nowrap' }}>
      <Cell>
        <Section>
          <Header>Section Header</Header>
          <SectionContent>
            <InputRow>
              <FullWidthInput placeholder="First Name" />
              <FullWidthInput placeholder="Last Name" />
            </InputRow>
            <InputRow>
              <FullWidthInput placeholder="Code" />
              <FullWidthInput placeholder="Phone Number" />
              <FullWidthInput placeholder="Email" />
            </InputRow>
            <InputRow>
              <FullWidthInput placeholder="Add a special request (optional)" />
            </InputRow>
          </SectionContent>
        </Section>
      </Cell>
      <Cell width={1 / 4}>
        <Section primary style={{ background: `linear-gradient( rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2) ), url(${water})`, backgroudRepeat: 'no-repeat', backgroundSize: 'cover' }}>
          <Header>
            <div>Section Header</div>
            <div>Test</div>
          </Header>
          <SectionContent>Content</SectionContent>
        </Section>
      </Cell>
      <Cell width={1 / 4}>
        <Section primary>
          <Header>Section Header</Header>
          <SectionContent>Content</SectionContent>
        </Section>
      </Cell>
    </Grid>
  </Container>
);

export default Main;
