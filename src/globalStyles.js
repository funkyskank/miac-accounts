import { injectGlobal } from 'styled-components';

injectGlobal`
  *, ::after, ::before {
    box-sizing: border-box;
    padding: 0;
  }

  body {
    background-color: #f4f4f4;
    color: #666;
    font-family: Roboto, "Helvetica Neue", Helvetica, Arial, sans-serif;
    line-height: 1.2;
    margin: 0;
  }
`;
