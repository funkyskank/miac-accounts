import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Wrapper = styled.div`
  position: relative;
  top: 2px;
  cursor: pointer;
  color: ${({ theme }) => theme.colors.subtitle};

  &:active {
    color: ${({ theme }) => theme.colors.text};
  }
`;

const Icon = styled.i.attrs({
  className: 'material-icons',
})`
  font-size: 18px;
`;

const IconButton = ({ children, ...props }) => (
  <Wrapper {...props}>
    <Icon>{children}</Icon>
  </Wrapper>
);

IconButton.propTypes = {
  children: PropTypes.node.isRequired,
};

export default IconButton;
