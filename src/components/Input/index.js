import styled from 'styled-components';

const Input = styled.input`
  height: 50px;
  padding: 0 20px;
  font: inherit;
  font-size: .875rem;
  color: ${({ theme }) => theme.colors.text};
  border: 2px solid ${({ theme }) => theme.colors.border};
  border-radius: 4px;
  outline: none;

  &::placeholder {
    color: ${({ theme }) => theme.colors.subtitle};
  }

  &:focus {
    border: 2px solid ${({ theme }) => theme.colors.blue};
  }
`;

export default Input;
