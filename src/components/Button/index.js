import styled, { css } from 'styled-components';

const Button = styled.button`
  font: inherit;
  font-weight: 500;
  font-size: 0.6875rem;
  color: #b3b3b3;
  background-color: #fdfdfd;
  padding: 10px 18px;
  margin-left: -2px;
  border: 2px solid ${({ theme }) => theme.colors.border};
  border-radius: 4px;
  text-transform: uppercase;
  white-space: nowrap;
  cursor: pointer;
  outline: none;
  letter-spacing: 1px;

  &:active {
    background-color: #f7f7f7;
  }

  ${props => props.primary && css`
    background-color: rgb(8, 111, 233);
    border: 2px solid rgb(8, 111, 233);
    color: #fff;

    &:active {
      background-color: rgb(0, 86, 208);
      border: 2px solid rgb(0, 86, 208);
    }
  `}
`;

export default Button;
