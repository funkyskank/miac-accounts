import styled, { css } from 'styled-components';

export const Grid = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;

  @media (min-width: 560px) {
      flex-direction: row;
  }
`;

export const Cell = styled.div`
  flex: 1 1 auto;
  display: flex;

  ${({ width }) => width && css`
    flex: ${`0 0 ${width * 100}%`};
  `}
`;
