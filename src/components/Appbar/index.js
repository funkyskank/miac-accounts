import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { withHandlers, compose } from 'recompose';
import { actions as toastActions } from 'reducers/toast';

import Button from 'components/Button';
import IconButton from 'components/IconButton';

const Wrapper = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: fixed;
  width: 100%;
  box-sizing: border-box;
  height: 88px;
  padding: 0 38px;
  background-color: #fdfdfd;
  box-shadow: 0 0 0 2px rgba(0, 0, 0, 0.1);
`;

const Appbar = ({ toast, error }) => (
  <Wrapper>
    <IconButton onClick={error('Red alert')}>menu</IconButton>
    <div>
      How To <em>Cook</em> With <strong>Fresh</strong> Herbs
    </div>
    <Button primary onClick={toast('Hello, toast test 😮😮😮')}>Sign In</Button>
  </Wrapper>
);

Appbar.propTypes = {
  toast: PropTypes.func.isRequired,
  error: PropTypes.func.isRequired,
};

const actions = {
  addToast: toastActions.addToast,
  errorToast: toastActions.errorToast,
};

export default compose(
  connect(null, actions),
  withHandlers({
    toast: ({ addToast }) => (text, color) => (event) => {
      const toastParams = { text };
      if (color) toastParams.color = color;
      addToast(toastParams);
    },
    error: ({ errorToast }) => text => (event) => {
      errorToast(text);
    },
  }),
)(Appbar);
