import React from 'react';
import { ThemeProvider } from 'styled-components';

export const colors = {
  text: '#666',
  subtitle: '#999',
  border: '#f0f0f0',

  '98 Gray': '#FAFAFA',
  '96 Gray': '#F5F5F5',
  '94 Gray': '#F0F0F0',
  '90 Gray': '#E6E6E6',
  '85 Gray': '#D9D9D9',
  '80 Gray': '#CCCCCC',
  '70 Gray': '#B3B3B3',
  '60 Gray': '#999999',
  '50 Gray': '#808080',
  '40 Gray': '#666666',
  '30 Gray': '#4D4D4D',
  '20 Gray': '#333333',
  '15 Gray': '#262626',
  '10 Gray': '#1A1A1A',
  '5 Gray': '#0D0D0D',
  blue: '#1875F0',
  green: '#50D166',
  red: '#F13A30',
  orange: '#F18F1C',
  purple: '#5553CE',
};

const fonts = {
  mainFont: 'Roboto',
};

export const theme = {
  colors,
  fonts,
};

const StyleThemeProvider = ({ children }) => (
  <ThemeProvider theme={theme}>
    {children}
  </ThemeProvider>
);

export default StyleThemeProvider;
