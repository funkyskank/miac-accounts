import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { withHandlers } from 'recompose';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { actions as toastActions } from 'reducers/toast';
import { TransitionGroup, Transition } from 'styled-transition-group-animation';

import Toast from './components/Toast';

const Wrapper = styled.ul`
  bottom: 24px;
  position: fixed;
  right: 24px;
  width: 240px;
`;

const ToastContainer = styled.div`
  &:not(:last-child) {
    margin: 0 0 12px;
  }
`;

const animation = {
  keyframes: `
    @keyframes bounceInRight {
      from, 60%, 75%, 90%, to {
        animation-timing-function: cubic-bezier(0.215, 0.610, 0.355, 1.000);
      }
      from {
        opacity: 0;
        transform: translate3d(3000px, 0, 0);
      }
      60% {
        opacity: 1;
        transform: translate3d(-25px, 0, 0);
      }
      75% {
        transform: translate3d(10px, 0, 0);
      }
      90% {
        transform: translate3d(-5px, 0, 0);
      }
      to {
        transform: none;
      }
    }

    @keyframes bounceOutRight {
      20% {
        opacity: 1;
        transform: translate3d(-20px, 0, 0);
      }
      to {
        opacity: 0;
        transform: translate3d(2000px, 0, 0);
      }
    }
    `,
  enter: 'bounceInRight',
  exit: 'bounceOutRight',
};

const Toasts = ({
  toasts,
  handlerDismiss,
}) => (
  <Wrapper>
    <TransitionGroup>
      {toasts.map((toast) => {
        const { id } = toast;
        return (
          <Transition key={id} type="bounce" animation={animation} duration={1000}>
            <ToastContainer>
              <Toast
                key={id}
                onDismiss={handlerDismiss(id)}
                {...toast}
              />
            </ToastContainer>
          </Transition>
        );
      })}
    </TransitionGroup>
  </Wrapper>
);

Toasts.propTypes = {
  toasts: PropTypes.array.isRequired,
  handlerDismiss: PropTypes.func.isRequired,
};

const actions = {
  removeToast: toastActions.removeToast,
};

const mapStateToProps = ({ toasts }) => ({
  toasts,
});

export default compose(
  connect(mapStateToProps, actions),
  withHandlers({
    handlerDismiss: ({ removeToast }) => id => (event) => {
      removeToast(id);
    },
  }),
)(Toasts);
