import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Wrapper = styled.li`
  align-items: flex-start;
  border-radius: 4px;
  color: #ffffff;
  display: flex;
  padding: 16px;
  background: ${props => props.bgColor};
`;

const Content = styled.p`
  flex: 1 1 auto;
  margin: 0 12px 0 0;
  overflow: hidden;
  text-overflow: ellipsis;
`;

const DismissButton = styled.button`
  -webkit-appearance: none;
  -moz-appearance: none;
  background: transparent;
  border: 0;
  color: inherit;
  cursor: pointer;
  display: block;
  flex: 0 0 auto;
  font: inherit;
  padding: 0;
`;

class Toast extends Component {
  componentDidMount() {
    this.timer = setTimeout(this.props.onDismiss, 5000);
  }

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  render() {
    return (
      <Wrapper bgColor={this.props.color}>
        <Content>
          {this.props.text}
        </Content>
        <DismissButton onClick={this.props.onDismiss}>
          x
        </DismissButton>
      </Wrapper>
    );
  }

  shouldComponentUpdate() {
    return false;
  }
}

Toast.propTypes = {
  onDismiss: PropTypes.func.isRequired,
  color: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
};

export default Toast;
