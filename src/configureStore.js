import { createStore, applyMiddleware, compose } from 'redux';
import { createLogger } from 'redux-logger';

import rootReducer from './reducers';

export default (initialState = {}) => {
  const middlewares = [
    createLogger({
      collapsed: (getState, action, logEntry) => !logEntry.error,
    }),
  ];

  const enhancers = [
    applyMiddleware(...middlewares),
  ];

  const store = createStore(
    rootReducer,
    initialState,
    compose(...enhancers),
  );

  return store;
};
