import nanoid from 'nanoid';

export const types = {
  TOAST_ADD: 'TOAST_ADD',
  TOAST_REMOVE: 'TOAST_REMOVE',
};

const defaultOptions = {
  color: '#6796e6',
};

const createToast = options => ({
  ...defaultOptions,
  ...options,
  id: nanoid(),
});


export const actions = {
  addToast: (options = {}) => ({
    type: types.TOAST_ADD,
    payload: createToast(options),
  }),
  errorToast: text => actions.addToast({ text, color: 'red' }),
  removeToast: id => ({ type: types.TOAST_REMOVE, payload: id }),
};

const initialState = [];

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.TOAST_ADD:
      return [payload, ...state];
    case types.TOAST_REMOVE:
      return state.filter(toast => toast.id !== payload);
    default:
      return state;
  }
};
