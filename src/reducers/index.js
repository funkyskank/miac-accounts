import { combineReducers } from 'redux';

import toasts from './toast';

const rootReducer = combineReducers({
  toasts,
});

export default rootReducer;
