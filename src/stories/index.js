import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import '../globalStyles';

import { Welcome } from '@storybook/react/demo';

import Button from '../components/Button';

storiesOf('Welcome', module).add('to Storybook', () => <Welcome showApp={linkTo('Button')} />);

storiesOf('Button', module)
  .add('default', () => <Button onClick={action('clicked')}>Button</Button>)
  .add('primary', () => <Button primary onClick={action('clicked')}>Primary</Button>);
