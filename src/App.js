import React from 'react';

import Appbar from 'components/Appbar';
import Toasts from 'containers/Toasts';
import Main from 'pages/Main';

const App = () => (
  <main>
    <Appbar />
    <Main />
    <Toasts />
  </main>
);

export default App;
